<?php
/**
 * class-groups-access-shortcodes.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is released under the GNU General Public License.
 * See COPYRIGHT.txt and LICENSE.txt.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups
 * @since groups 1.0.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Shortcode handlers.
 */
class Groups_Access_Shortcodes {

	/**
	 * Defines access shortcodes.
	 */
	public static function init() {

		// group restrictions
		add_shortcode( 'groups_member', array( __CLASS__, 'groups_member' ) );
		add_shortcode( 'groups_non_member', array( __CLASS__, 'groups_non_member' ) );

		// capabilities
		add_shortcode( 'groups_can', array( __CLASS__, 'groups_can' ) );
		add_shortcode( 'groups_can_not', array( __CLASS__, 'groups_can_not' ) );
	}

	/**
	 * Takes one attribute "group" which is a comma-separated list of group
	 * names or ids (can be mixed).
	 * The content is shown if the current user belongs to the group(s).
	 *
	 * @param array $atts attributes
	 * @param string $content content to render
	 */
	public static function groups_member( $atts, $content = null ) {
		$output = "";
		$options = shortcode_atts( array( "group" => "" ), $atts );
		$show_content = false;
		if ( $content !== null ) {
			$groups_user = new Groups_User( get_current_user_id() );
			$groups = explode( ",", $options['group'] );
			foreach ( $groups as $group ) {
				$group = trim( $group );
				$current_group = Groups_Group::read( $group );
				if ( !$current_group ) {
					$current_group = Groups_Group::read_by_name( $group );
				}
				if ( $current_group ) {
					if ( Groups_User_Group::read( $groups_user->user->ID , $current_group->group_id ) ) {
						$show_content = true;
						break;
					}
				}
			}
			if ( $show_content ) {
				remove_shortcode( 'groups_member' );
				$content = do_shortcode( $content );
				add_shortcode( 'groups_member', array( __CLASS__, 'groups_member' ) );
				$output = $content;
			} else { // CUSTOM CODE FOR VIDEO OVERLAY
				?>
				<div style="">

					<div id="videopreview" >
						<div id="overlayblock1">
							<?php
							$shows_cats = get_categories('child_of=54');
							$shows_cats[] = get_category(271); // Adding the GC Events category to use here the shows block overlay also for this videos
							$shows_cats_ids = array();

							foreach ( $shows_cats as $show_cat ) {
								$shows_cats_ids[] = $show_cat->term_id;
							}
							?>

							<?php if ( has_category( $shows_cats_ids, $post->ID ) ): ?>
								<!-- <div class="row text-center">
								<img src="<?php //echo get_site_url(); ?>/wp-content/uploads/GCTV_2015.06.22_GCTV_Logo.png">
							</div> -->
							<div class="row text-center">
								<span class="exclusive-text">Exclusive</span>
								<span class="premium-text">Subscribe for Free Access & Sneak Preview <br />
									Premiere of Whatever it Takes Season 2 <br />
									Friday April 1st at 10pm EST</span>
								</div>
								<div class="row text-center">
									<a class="btn login-modal" href="#" data-toggle="modal" data-target="#myModal2">Click Here To Watch</a>
								</div>
							<?php else: ?>
							<div class="row valignmiddle">
								<div>
									<h2 class="membersonlyh1"><?php
										$path = $_SERVER['REQUEST_URI'];
										$find = '10x-seminar';
										$pos = strpos($path, $find);
										if ($pos !== false)
										{
											echo 'Sign-up for our 10X membership plan to watch!';
										} elseif (is_single(50538)) { ?>
											<div class="webcast-description">
												<span>In this LIVE video webcast, you will learn:</span>
												<ul>
													<li>Grant’s EXCLUSIVE 365 Day Follow-Up Plan</li>
													<li>17 reasons people WILL NOT BUY from you</li>
													<li>How prospecting is different from selling</li>
													<li>6 ways to SUPERCHARGE your prospecting</li>
													<li>9 of your HOTTEST leads you don’t know about</li>
													<li>The 1-CALL close</li>
													<li>And more…</li>
												</ul>
											</div>
											<?php $btn_signup_text = 'SIGN UP NOW'; ?>
											<?php } elseif ( is_single( 64120 ) ) {  ?>
												<div class="webcast-description">
													<span>10X Super Life Webinar is Paid Content</span>
													<ul>
														<li>On Demand 24/7</li>
														<li>10X Your Income</li>
														<li>10X Your Business</li>
														<li>10X Your Career</li>
													</ul>
												</div>
												<?php $btn_signup_text = 'SIGN UP NOW'; ?>
												<?php
										} elseif ( !is_single(9144) &&	// Secrets To Closing The Sale
											!is_single(7593) && 	// 7 Sales Secrets Webinar
											!is_single(50538) &&	// How to Make Millions on the Phone Webcast
											!is_single(55605) &&	// 3 Days to a Raise Video Seminar
											!is_single(60638) &&	// 10X Everything Exercises
											!is_single(74251) && 	// How to Make Millions in Business
											!is_single(74234) && 	// Sell or Be Sold Bundle
											!is_single(79879) &&	// The Greatest Sales Secret
											!is_single( array( 6288, 6302 ) ) &&	// 10X Seminar Cancun
											!is_single( array( 83641, 83659 ) )		// Millionaire Now
											) {
											echo 'This is FREE Content.';
											$btn_signup_text = 'SUBSCRIBE NOW';
										} elseif ( is_single( 60638 ) ) {
											echo 'This is Paid Content.';
											$btn_signup_text = 'GET ACCESS';
										} else {
											echo 'This is Premium Content.';
											$btn_signup_text = 'SIGN UP NOW';
										}

										?></h2>
									</div>
									<div class="row push-top">
										<div class="col-sm-4">
											<a href="<?php
											$path = $_SERVER['REQUEST_URI'];
											$find = '10x-seminar';
											$pos = strpos($path, $find);
											if ($pos !== false) {
												echo '/10xlive';
											} elseif ( is_single( 9144 ) ) { // Secrets To Closing The Sale
												echo '/secrets-to-closing-signup';
											} elseif ( is_single( 7593 ) ) { // 7 Sales Secrets Webinar
												echo '/top-sales-secrets-signup';
											} elseif ( is_single( 50538 ) ) { // How to Make Millions on the Phone Webcast
												echo '/millions-on-the-phone-signup';
											} elseif ( is_single( 55605 ) ) { // 3 Days to a Raise Video Seminar
												echo '/product/3-days-to-a-raise-seminar';
											} elseif ( is_single( 60638 ) ) { // 10X Everything Exercises
												echo 'http://grantcardonetv.com/cart/?add-to-cart=58265';
											} elseif ( is_single( 64120 ) ) { // 10X Everything
												echo '/10x-superlife-signup';
											} elseif ( is_single( 74251 ) ) { // How to Make Millions in Business
												echo '/millions-in-business-signup';
											} elseif ( is_single( 74234 ) ) { // Sell or Be Sold Bundle
												echo 'http://www.grantcardone.com/sellorbesold';
											} elseif ( is_single( 79879 ) ) { // The Greatest Sales Secret
												echo '/greatest-sales-secret-signup';
											} elseif ( is_single( array( 6288, 6302 ) ) ) { // 10X Seminar Cancun
												echo '/10x-live-seminar-signup';
											} elseif ( is_single( array( 83641, 83659 ) ) ) { // Millionaire Now
												echo '/millionairenowcart';
											} else {
												echo '/subscribe';
											}
											?>" class="previewbutton btn" id="free" ><?php echo $btn_signup_text; ?></a>
											<?php //if (is_single(50538)): ?>
											<!-- <a class="millionsonthephone-learnmore" href="http://millionsonthephone.com/">Learn more</a> -->
											<?php //endif ?>
										</div>

										<div class="col-sm-4">

										</div>

										<div class="col-sm-4">
											&nbsp;
										</div>
									</div>
									<?php if ( !is_user_logged_in() ): ?>
									<div class="row extralinksvideo">
										<div class="smallwhiteplayertext ">Already
											<?php if ( is_single( array( 
													50538,	// How to Make Millions on the Phone Webcast
													64120,	// 10X Everything
													74251,	// How to Make Millions in Business
													79879	// The Greatest Sales Secret
													) ) ) {
												echo 'registered for the Webcast';
											} elseif ( is_single( 60638 ) ) {
												echo 'have access';
											} else {
												echo 'have an account with us';
											}
											?>? <a class="btn btn-xs" href="#" data-toggle='modal' data-target='#myModal2'>LOGIN</a></div>
										</div>
									<?php endif ?>
								</div>
							<?php endif ?>
						</div>
						<?php echo $content; ?>
					</div>
				</div>

				<?php

				global $post;

// load all 'category' terms for the post
				$terms = get_the_terms($post->ID, 'category');

// we will use the first term to load ACF data from
				if( !empty($terms) ) {
					$term = array_pop($terms);
					$availablelist = get_field('available_to', $term );
					$blocked = get_field('blocked', $term );
				}

				?>

				<?php }
			}
			return $output;
		}

	/**
	 * Takes one attribute "group" which is a comma-separated list of group
	 * names or ids (can be mixed).
	 * The content is shown if the current user does NOT belong to the group(s).
	 *
	 * @param array $atts attributes
	 * @param string $content content to render
	 */
	public static function groups_non_member( $atts, $content = null ) {
		$output = "";
		$options = shortcode_atts( array( "group" => "" ), $atts );
		$show_content = true;
		if ( $content !== null ) {
			$groups_user = new Groups_User( get_current_user_id() );
			$groups = explode( ",", $options['group'] );
			foreach ( $groups as $group ) {
				$group = trim( $group );
				$current_group = Groups_Group::read( $group );
				if ( !$current_group ) {
					$current_group = Groups_Group::read_by_name( $group );
				}
				if ( $current_group ) {
					if ( Groups_User_Group::read( $groups_user->user->ID , $current_group->group_id ) ) {
						$show_content = false;
						break;
					}
				}
			}
			if ( $show_content ) {
				remove_shortcode( 'groups_non_member' );
				$content = do_shortcode( $content );
				add_shortcode( 'groups_non_member', array( __CLASS__, 'groups_non_member' ) );
				$output = $content;
			}
		}
		return $output;
	}

	/**
	 * Takes one attribute "capability" that must be a valid capability label
	 * or a list of capabilities separated by comma.
	 * The content is shown if the current user has one of the capabilities.
	 *
	 * @param array $atts attributes
	 * @param string $content content to render
	 */
	public static function groups_can( $atts, $content = null ) {
		$output = "";
		$options = shortcode_atts( array( "capability" => "" ), $atts );
		if ( $content !== null ) {
			$groups_user = new Groups_User( get_current_user_id() );
			$capability = $options['capability'];
			$capabilities = array_map( 'trim', explode( ',', $capability ) );
			$show_content = false;
			foreach( $capabilities as $capability ) {
				if ( $groups_user->can( $capability ) ) {
					$show_content = true;
					break;
				}
			}
			if ( $show_content ) {
				remove_shortcode( 'groups_can' );
				$content = do_shortcode( $content );
				add_shortcode( 'groups_can', array( __CLASS__, 'groups_can' ) );
				$output = $content;
			}
		}
		return $output;
	}

	/**
	 * Takes one attribute "capability" that must be a valid capability label,
	 * or a comma-separaed list of those.
	 * The content is shown if the current user has none of the capabilities.
	 *
	 * @param array $atts attributes
	 * @param string $content content to render
	 */
	public static function groups_can_not( $atts, $content = null ) {
		$output = "";
		$options = shortcode_atts( array( "capability" => "" ), $atts );
		if ( $content !== null ) {
			$groups_user = new Groups_User( get_current_user_id() );
			$capability = $options['capability'];
			$capabilities = array_map( 'trim', explode( ',', $capability ) );
			$show_content = true;
			foreach( $capabilities as $capability ) {
				if ( $groups_user->can( $capability ) ) {
					$show_content = false;
					break;
				}
			}
			if ( $show_content ) {
				remove_shortcode( 'groups_can_not' );
				$content = do_shortcode( $content );
				add_shortcode( 'groups_can_not', array( __CLASS__, 'groups_can_not' ) );
				$output = $content;
			}
		}
		return $output;
	}
}
Groups_Access_Shortcodes::init();
